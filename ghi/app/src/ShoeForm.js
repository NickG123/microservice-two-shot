import React from "react";


class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: '',
        bins:[],
        };
        //this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModelNameChange = this.handleModelNameChange.bind(this);
        this.handleShoeColorChange = this.handleShoeColorChange.bind(this);
        this.handleShoePhotoUrlChange = this.handleShoePhotoUrlChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data.bins)
            this.setState({bins: data.bins})
        }
    }




    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        // data.manufacturer = data.manufacturer1;
        // data.model_name = data.model_name1;
        // data.color = data.color1;
        // data.picture_url = data.picture_url1;
        // delete data.manufacturer1;
        // delete data.model_name1;
        // delete data.color1;
        // delete data.picture_url1;
        delete data.bins;
        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);

        if (response.ok) {

            const newshoe = await response.json();

            this.setState({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
            });

        };
        // this.setState({
        //     manufacturer: '',
        //     model_name: '',
        //     color: '',
        //     picture_url: '',
        //     bin: '',
        // })
    }
    // async handleInput(event){
    //     this.setState({
    //         [event.target.name]: event.target.value
    //     })
    // }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
    }

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({ model_name: value });
    }

    handleShoeColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handleShoePhotoUrlChange(event) {
        const value = event.target.value;
        this.setState({ picture_url: value})
    }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({ bin: value })
    }

    render ()
    {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="manufacturer">Manufacturer</label>
                        <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} required type="text" name="manufacturer" id="manufacturer" className="form-control" />

                    </div>
                    <div>
                        <label htmlFor="model_name">Model Name</label>
                        <input onChange={this.handleModelNameChange} value={this.state.model_name} required type="text" name="model_name" id="model_name" className="form-control" />

                    </div>
                    <div>
                        <label htmlFor="color">Color</label>
                        <input onChange={this.handleShoeColorChange} value={this.state.color} required type="text" id="color" name="color"className="form-control"/>
                    </div>
                    <div>
                        <label htmlFor="picture_url">Picture URL</label>
                        <input onChange={this.handleShoePhotoUrlChange} value={this.state.picture_url} id="picture_url" required type="text" name="picture_url"className="form-control"/>
                    </div>
                    <div>
                        <label>Bin</label>
                        {/* <input onChange={this.handleBinChange} value={this.state.bin} id="bin"  name="bin" className="form-control"/> */}
                        <select onChange={this.handleBinChange} required name="bin" id="bin" className="form-select">
                            <option value="">Choose a location</option>

                            {this.state.bins.map(bin => {
                                {console.log(bin)}

                                return (
                                    <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                                )
                            })}
                        </select>
                    </div>

                    <button>Create Shoe</button>
                </form>
            </div>

        );
    }
}
export default ShoeForm;
