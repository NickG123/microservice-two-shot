import {useState, useEffect} from 'react';


function ShoeList() {
    const [shoes, setShoes] = useState([]);
    console.log('test')
    console.log(shoes)


    useEffect(() => {
        const getData = async () =>{
            const resp = await fetch ('http://localhost:8080/api/shoes/')
            const data = await resp.json();
            console.log(data)
            setShoes(data.shoes)

        }
        getData();
    }, [])



    return (
        <table>
            <thead>
                <th>manufacturer</th>
                <th>model_name</th>
                <th>color</th>
                <th>picture_url</th>
                <th>bin</th>
            </thead>
        <tbody>
        {
            shoes.map(shoe=> {
                return(<tr>
                    <td>{shoe.pk}</td>
                    <td>{shoe.manufacturer}</td>
                    <td>{shoe.model_name}</td>
                    <td>{shoe.color}</td>
                    <td><img src={shoe.picture_url} width="100px" length= "100px"/></td>
                    <td>{shoe.bin}</td>
                </tr>)
            })
        }
        </tbody>
        </table>
    )

}
export default ShoeList;
