from django.contrib import admin

from .models import Location, Bin


@admin.register(Location)
class Location(admin.ModelAdmin):
    pass

@admin.register(Bin)
class ShoeAdmin(admin.ModelAdmin):
    pass
