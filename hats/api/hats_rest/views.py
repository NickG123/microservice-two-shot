from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from common.json import ModelEncoder
from django.http import JsonResponse
import json

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name","import_href",]

class Hat_List_Encoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name","color", "picture_url", "pk", ]

    def get_extra_data(self, o):
        return {"location": o.location.name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name","color", "picture_url", "location",]

    encoders = {
        "location": LocationVODetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"location": o.location.name}




@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=Hat_List_Encoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"},status=400,)

        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False,
        )
@require_http_methods(["DELETE", "GET",])
def api_show_hat(request, pk):
    if request.method =="GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False,
        )
    else:
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
