from django.shortcuts import render
from .models import BinVO, Shoe
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name","import_href",]

class Shoe_List_Encoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name","color", "picture_url", "pk", ]

    def get_extra_data(self, o):
        return {"bin": o.bin.name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name","color", "picture_url", "bin",]

    encoders = {
        "bin": BinVODetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"bin": o.bin.name}




@require_http_methods(["GET", "POST"])
def api_list_shoes(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            shoes = Shoe.objects.filter(bin=location_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=Shoe_List_Encoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"},status=400,)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False,
        )

def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False,)
    else:
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
